package com.fleapx.netty_learn.learn_beat;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @version:: netty_heart
 * @description:
 * @create: 2021/12/3 2:44.
 */
public class BeatClientHandler extends SimpleChannelInboundHandler<String> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String s) throws Exception {
        System.out.println("----------------->[clent] message received:"+s);
        if(s!=null &&"you are later".equals(s)){
            System.out.println("[server] was closed connection ,so you will cloed too");
            ctx.channel().closeFuture();
        }
    }
}
