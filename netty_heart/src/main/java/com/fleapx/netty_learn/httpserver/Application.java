package com.fleapx.netty_learn.httpserver;

/**
 * 2021/11/2.
 */
public class Application {

    public static void main(String[] args) throws Exception{
        HttpServer server = new HttpServer(8081);
        server.start();
    }
}
