package com.fleapx.netty_learn.chapter13;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * Created by dimi on 2021/12/12.
 */
public class LogEventHandler extends SimpleChannelInboundHandler<LogEvent> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, LogEvent msg) throws Exception {
        StringBuffer sb = new StringBuffer();
        sb.append(msg.getReceivedTimestamp());
        sb.append("[");
        sb.append(msg.getSource().toString());
        sb.append("] [");
        sb.append(msg.getLogfile());
        sb.append("] : ");
        sb.append(msg.getMsg());
        System.out.println(sb.toString());
    }
}
