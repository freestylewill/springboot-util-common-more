package com.fleapx.netty_learn.chapter12;

/**
 * 2021/11/22.
 */
public class SpdyRequestHandler  extends HttpRequestHandler {

    @Override
    protected String getContent() {
        return "This content is transmitted via SPDY\r\n";
    }

}
