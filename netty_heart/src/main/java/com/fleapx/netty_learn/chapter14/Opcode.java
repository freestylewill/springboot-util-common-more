package com.fleapx.netty_learn.chapter14;

/**
 * Created by dimi on 2021/12/12.
 */
public class Opcode {

    public static final byte GET = 0x00;
    public static final byte SET = 0x01;
    public static final byte DELETE = 0x04;

}
