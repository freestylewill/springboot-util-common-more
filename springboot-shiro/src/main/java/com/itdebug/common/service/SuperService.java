package com.itdebug.common.service;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.itdebug.entity.User;
import com.itdebug.exception.MyException;
import com.itdebug.status.StatusInfoEnum;
import org.apache.shiro.SecurityUtils;

/**
 * @author itdebug
 * @description service 层超类
 * @create 2021-12-27 15:24
 */
public class SuperService<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> {

    /**
     * 获取当前用户
     * 
     * @return
     */
    public User getCurrentUser() {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        
        if (user == null) {
            throw new MyException(StatusInfoEnum.REQUEST_UNAUTHC_EXCEPTION);
        }
        
        return user;
    }
}
