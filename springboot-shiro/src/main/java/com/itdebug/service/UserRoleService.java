package com.itdebug.service;

import com.itdebug.entity.UserRole;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户-角色中间表 服务类
 * </p>
 *
 * @author itdebug
 * @since 2021-12-26
 */
public interface UserRoleService extends IService<UserRole> {

}
