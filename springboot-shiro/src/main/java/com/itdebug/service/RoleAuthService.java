package com.itdebug.service;

import com.itdebug.entity.RoleAuth;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 角色-权限关联表 服务类
 * </p>
 *
 * @author itdebug
 * @since 2021-12-26
 */
public interface RoleAuthService extends IService<RoleAuth> {

}
