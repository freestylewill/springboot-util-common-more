package com.itdebug.service;

import com.itdebug.entity.Role;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author itdebug
 * @since 2021-12-26
 */
public interface RoleService extends IService<Role> {

}
