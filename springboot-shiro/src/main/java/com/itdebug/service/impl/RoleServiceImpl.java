package com.itdebug.service.impl;

import com.itdebug.entity.Role;
import com.itdebug.mapper.RoleDao;
import com.itdebug.service.RoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author itdebug
 * @since 2021-12-26
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleDao, Role> implements RoleService {

}
