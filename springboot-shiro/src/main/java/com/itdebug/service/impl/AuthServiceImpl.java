package com.itdebug.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.itdebug.entity.Auth;
import com.itdebug.exception.MyException;
import com.itdebug.mapper.AuthDao;
import com.itdebug.service.AuthService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户权限表 服务实现类
 * </p>
 *
 * @author itdebug
 * @since 2021-12-27
 */
@Service
public class AuthServiceImpl extends ServiceImpl<AuthDao, Auth> implements AuthService {

    @Override
    public List<Auth> getUserAuthListById(Long id) {
        if (null == id) {
            throw new MyException("未知异常，请联系");
        }
        
        return super.baseMapper.getUserAuthListById(id);
    }
}
