package com.itdebug.service.impl;

import com.itdebug.entity.UserRole;
import com.itdebug.mapper.UserRoleDao;
import com.itdebug.service.UserRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户-角色中间表 服务实现类
 * </p>
 *
 * @author itdebug
 * @since 2021-12-26
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleDao, UserRole> implements UserRoleService {

}
