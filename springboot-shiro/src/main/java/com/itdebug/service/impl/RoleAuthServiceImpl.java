package com.itdebug.service.impl;

import com.itdebug.entity.RoleAuth;
import com.itdebug.mapper.RoleAuthDao;
import com.itdebug.service.RoleAuthService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色-权限关联表 服务实现类
 * </p>
 *
 * @author itdebug
 * @since 2021-12-26
 */
@Service
public class RoleAuthServiceImpl extends ServiceImpl<RoleAuthDao, RoleAuth> implements RoleAuthService {

}
