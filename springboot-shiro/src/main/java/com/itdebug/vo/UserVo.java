package com.itdebug.vo;

import com.itdebug.entity.Auth;
import com.itdebug.entity.User;

import java.util.List;

/**
 * @author itdebug
 * @description 用户信息 vo
 * @create 2021-12-26 21:11
 */
public class UserVo extends User {
    /**
     * 用户权限
     */
    private List<Auth> authList;

    public List<Auth> getAuthList() {
        return authList;
    }

    public void setAuthList(List<Auth> authList) {
        this.authList = authList;
    }

    @Override
    public String toString() {
        return "UserVo{" +
                "authList=" + authList +
                '}';
    }
}
