package com.itdebug.mapper;

import com.itdebug.entity.UserRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户-角色中间表 Mapper 接口
 * </p>
 *
 * @author itdebug
 * @since 2021-12-26
 */
public interface UserRoleDao extends BaseMapper<UserRole> {

}
