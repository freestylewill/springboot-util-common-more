package com.itdebug.mapper;

import com.itdebug.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户数据表 Mapper 接口
 * </p>
 *
 * @author itdebug
 * @since 2021-12-26
 */
public interface UserDao extends BaseMapper<User> {

}
