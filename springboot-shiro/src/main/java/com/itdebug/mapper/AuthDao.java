package com.itdebug.mapper;

import com.itdebug.entity.Auth;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 用户权限表 Mapper 接口
 * </p>
 *
 * @author itdebug
 * @since 2021-12-27
 */
public interface AuthDao extends BaseMapper<Auth> {

    /**
     * 根据用户 id 获取权限信息
     * 
     * @param id
     * @return
     */
    List<Auth> getUserAuthListById(Long id);
}
