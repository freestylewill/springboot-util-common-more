package com.itdebug.mapper;

import com.itdebug.entity.Role;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author itdebug
 * @since 2021-12-26
 */
public interface RoleDao extends BaseMapper<Role> {

}
