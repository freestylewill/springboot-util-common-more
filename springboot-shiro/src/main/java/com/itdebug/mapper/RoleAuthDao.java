package com.itdebug.mapper;

import com.itdebug.entity.RoleAuth;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 角色-权限关联表 Mapper 接口
 * </p>
 *
 * @author itdebug
 * @since 2021-12-26
 */
public interface RoleAuthDao extends BaseMapper<RoleAuth> {

}
