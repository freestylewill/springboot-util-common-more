package com.itdebug;

import com.fleapx.log.itdebug.annotion.EnableMyLog;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 入口类
 *
 * @author itdebug
 * @date 2021/12/18
 */
@EnableMyLog
@SpringBootApplication(scanBasePackages = "com.itdebug")
public class AreaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AreaApplication.class, args);
	}
}
