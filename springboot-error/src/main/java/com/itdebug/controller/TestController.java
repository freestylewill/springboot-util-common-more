package com.itdebug.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author itdebug
 * @create 2021-12-20 8:49
 **/
@Controller
public class TestController {
    
    @RequestMapping("/test")
    @ResponseBody
    public String test() {
        int i = 1 / 0;
        return "Hello Spring Boot!";
    }
}
