package com.fleapx.log.itdebug.annotion;

import com.fleapx.log.itdebug.config.LogMarkerConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(LogMarkerConfiguration.class)
public @interface EnableMyLog {
}
