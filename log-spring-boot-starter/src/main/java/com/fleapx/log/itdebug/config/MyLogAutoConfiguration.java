package com.fleapx.log.itdebug.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(MylogProperties.class)
@ConditionalOnBean(LogMarkerConfiguration.class)
public class MyLogAutoConfiguration {
    @Bean
    public MyLogAspect myLogAspect(@Autowired MylogProperties mylogProperties){
        return new MyLogAspect(mylogProperties);
    }
}
