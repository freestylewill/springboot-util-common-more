package com.fleapx.log.itdebug.config;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
//@Component
public class MyLogAspect {
    private MylogProperties mylogProperties;
    public MyLogAspect(MylogProperties mylogProperties) {
        this.mylogProperties=mylogProperties;
    }

    @Pointcut("@annotation(com.fleapx.log.itdebug.annotion.MyLogAnnotation)")
    public void logAnnotationAnnotationPointcut() {
    }
    @Around("logAnnotationAnnotationPointcut()")
    public Object logInvoke(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println(mylogProperties.getOrDefualtPrefix("请求参数")+ Arrays.toString(joinPoint.getArgs()));
        Object obj = joinPoint.proceed();
        System.out.println(mylogProperties.getOrDefualtSubfix("返回值：")+ obj.toString());
        return obj;
    }


}
