package com.fleapx.log.itdebug.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.StringUtils;

@ConfigurationProperties(value ="mylog")
public class MylogProperties {
    //日志前缀
    private String prefix;
    //日志后缀
    private String subfix;


    public String getOrDefualtPrefix(String defualtPrefix){
        if(StringUtils.isEmpty(prefix)){
            return defualtPrefix;
        }
        return prefix;
    }

    public String getOrDefualtSubfix(String defualtSubfix){
        if(StringUtils.isEmpty(subfix)){
            return defualtSubfix;
        }
        return subfix;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSubfix() {
        return subfix;
    }

    public void setSubfix(String subfix) {
        this.subfix = subfix;
    }


}
