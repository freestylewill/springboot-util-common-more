package com.itdebug.mapper;

import com.itdebug.bean.Employee;

/**
 * @author itdebug
 * @create 2021-12-22 9:37
 **/
public interface EmployeeMapper {
    Employee getEmpById(Integer id);
}
