package com.itdebug.mapper;

import com.itdebug.bean.Employee;
import org.apache.ibatis.annotations.Select;

/**
 * @author itdebug
 * @create 2021-12-22 9:37
 **/
public interface EmployeeMapper {

    @Select("SELECT * FROM t_emp WHERE emp_id = #{id}")
    Employee getEmpById(Integer id);
}
