package com.itdebug.service;

import com.itdebug.bean.Employee;
import com.itdebug.mapper.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author itdebug
 * @create 2021-12-22 9:44
 **/
@Service
public class EmployeeService {
    
    @Autowired
    private EmployeeMapper employeeMapper;
    
    public Employee getEmpById(Integer id) {
        return employeeMapper.getEmpById(id);
    }
}
