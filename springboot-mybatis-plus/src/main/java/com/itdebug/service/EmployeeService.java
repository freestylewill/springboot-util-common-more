package com.itdebug.service;

import com.itdebug.common.MyBasePageRequest;
import com.itdebug.common.MyBasePageResponse;
import com.itdebug.entity.Employee;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author itdebug
 * @since 2021-12-21
 */
public interface EmployeeService extends IService<Employee> {

    /**
     * 分页获取员工信息
     * 
     * @param request
     * @return
     */
    MyBasePageResponse<Employee> getEmpPage(MyBasePageRequest<Employee> request);
}
