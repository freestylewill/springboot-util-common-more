package com.itdebug.mapper;

import com.itdebug.entity.Employee;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author itdebug
 * @since 2021-12-21
 */
public interface EmployeeDao extends BaseMapper<Employee> {

}
