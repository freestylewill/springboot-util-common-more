package com.fleapx.pattern.strategy.impl;

import com.fleapx.pattern.strategy.Strategy;
import org.springframework.stereotype.Component;

/**
 * @author itdebug
 * @Date 2021-12-31
 */
@Component("one")
public class StrategyOne implements Strategy {
    @Override
    public String doOperation() {
        return "one";
    }
}
