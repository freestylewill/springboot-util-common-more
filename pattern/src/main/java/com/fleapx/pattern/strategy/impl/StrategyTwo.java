package com.fleapx.pattern.strategy.impl;

import com.fleapx.pattern.strategy.Strategy;
import org.springframework.stereotype.Component;

/**
 * @author itdebug
 * @Date 2021-12-31
 */
@Component("two")
public class StrategyTwo implements Strategy {
    @Override
    public String doOperation() {
        return "two";
    }
}
