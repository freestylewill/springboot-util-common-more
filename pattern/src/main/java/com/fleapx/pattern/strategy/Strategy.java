package com.fleapx.pattern.strategy;

/**
 * @author itdebug
 * @Date 2021-12-31
 */
public interface Strategy {

    String doOperation();

}
