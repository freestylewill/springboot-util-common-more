package com.itdebug.spring.test;

import com.itdebug.spring.SpringExtendBeanApplicationTests;
import com.itdebug.spring.bean.ISuper;
import com.itdebug.spring.constant.BeanType;
import com.itdebug.spring.service.ObjService;
import com.itdebug.spring.strategy.MyAware;
import com.itdebug.spring.strategy.MyListener;
import com.itdebug.spring.strategy.other.MyDispatcher;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Test
 *
 * @author itdebug
 * @since 2021-12-01
 */
public class TestClass extends SpringExtendBeanApplicationTests {
//    @Resource
    @Autowired
    ObjService objService;

    @Test
    public void listenerTest() {
        ISuper classA = MyListener.getSuperMap(BeanType.CLASS_A.getBeanType());
        System.out.println(classA);
    }

    @Test
    public void awareTest() {
        ISuper classA = MyAware.getSuperMap(BeanType.CLASS_A.getBeanType());
        System.out.println(classA);
    }

    @Test
    public void dispatcherTest() {
        ISuper classB = MyDispatcher.getSuperMap(BeanType.CLASS_B.getBeanType());
        System.out.println(classB);
    }
    @Test
    public void addService() {
        System.out.println(objService.addService(3,5));
    }
    @Test
    public void divService() {
        System.out.println(objService.divService(3,0));
    }
}
