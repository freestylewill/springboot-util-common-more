package com.itdebug.spring.config;

import com.itdebug.spring.custom.FoodInfoResolver;
import com.itdebug.spring.custom.UserInfoResolver;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@Configuration
public class ConfigAdapter extends WebMvcConfigurerAdapter {
 
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        super.addArgumentResolvers(argumentResolvers);
        argumentResolvers.add(new UserInfoResolver());
        //当然可以增加一连串的解析器
        argumentResolvers.add(new FoodInfoResolver());
    }
}