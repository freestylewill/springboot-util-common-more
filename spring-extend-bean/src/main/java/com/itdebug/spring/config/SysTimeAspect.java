package com.itdebug.spring.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

@Configuration
@EnableAspectJAutoProxy // 开启aop 相当于 <aop:aspectj-autoproxy proxy-target-class="true"/>
@Aspect // 切面
@Component // 把普通pojo实例化到spring容器中，相当于配置文件中的<bean id="" class=""/>
public class SysTimeAspect {

    @Before("doTime()")
    public void doBefore(JoinPoint jp) {
        System.out.println("time doBefore()");
    }

    /**
     * 切入点
     */
//    @Pointcut("bean(sysMenuServiceImpl)")
    @Pointcut("execution( * *Service*(..))")
//    @Pointcut("within(com.itdebug.spring.service.impl.BookService)")
//    @Pointcut("bean(*Service)")
    public void doTime() {
    }

    @After("doTime()")
    public void doAfter() {//类似于finally{}代码块
        System.out.println("time doAfter()");
    }

    /**
     * 核心业务正常结束时执行
     * 说明：假如有after，先执行after,再执行returning
     */
    @AfterReturning("doTime()")
    public void doAfterReturning() {
        System.out.println("time doAfterReturning");
    }

    /**
     * 核心业务出现异常时执行
     * 说明：假如有after，先执行after,再执行Throwing
     */
    @AfterThrowing("doTime()")
    public void doAfterThrowing() {
        System.out.println("time doAfterThrowing");
    }

    @Around("doTime()")
    public Object doAround(ProceedingJoinPoint jp)
            throws Throwable {
        System.out.println("doAround.before");
        try {
            Object obj = jp.proceed();
            return obj;
        } catch (Throwable e) {
            System.out.println("doAround.error-->" + e.getMessage());
            throw e;
        } finally {
            System.out.println("doAround.after");
        }
    }

}