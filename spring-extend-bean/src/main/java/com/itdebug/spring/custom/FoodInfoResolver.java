package com.itdebug.spring.custom;

import com.itdebug.spring.bean.FoodInfo;
import com.itdebug.spring.bean.UserInfo;
import com.itdebug.spring.meta.FoodId;
import com.itdebug.spring.meta.UserId;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;

public class FoodInfoResolver implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.hasParameterAnnotation(FoodId.class);
    }
 
    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
        HttpServletRequest request = nativeWebRequest.getNativeRequest(HttpServletRequest.class);
        Long foodId = Long.valueOf(request.getHeader("foodId"));
        //查询数据库或缓存
        FoodInfo foodInfo = new FoodInfo();
        foodInfo.setFoodId(foodId);
        foodInfo.setFoodName("tomtom");
        return foodInfo;
    }
}