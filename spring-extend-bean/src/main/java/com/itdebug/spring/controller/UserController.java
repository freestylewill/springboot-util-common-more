package com.itdebug.spring.controller;

import com.itdebug.spring.bean.FoodInfo;
import com.itdebug.spring.bean.UserInfo;
import com.itdebug.spring.meta.FoodId;
import com.itdebug.spring.meta.UserId;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
 
    @RequestMapping("/info")
    public void getUserInfo(@UserId UserInfo userInfo, @FoodId FoodInfo foodInfo) {
        System.out.println(userInfo);
        System.out.println(foodInfo);
    }
}