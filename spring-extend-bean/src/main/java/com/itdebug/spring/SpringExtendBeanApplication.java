package com.itdebug.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication  //(excludeName = "NoOpCacheConfiguration,TaskSchedulingAutoConfiguration")
public class SpringExtendBeanApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringExtendBeanApplication.class, args);
    }

}
