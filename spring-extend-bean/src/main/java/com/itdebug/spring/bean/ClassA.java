package com.itdebug.spring.bean;

import com.itdebug.spring.constant.BeanType;
import org.springframework.stereotype.Component;

/**
 * ClassA
 *
 * @author itdebug
 * @since 2021-12-01
 */
@Component
public class ClassA implements ISuper {

    @Override
    public String getType() {
        return BeanType.CLASS_A.getBeanType();
    }
}
