package com.itdebug.spring.bean;

/**
 * @version:: springboot-util-common-more
 * @description:
 * @create: 2022/1/11 19:29.
 */
public class UserInfo {
    Long userId;
    private String userName;
    private int userAge;

    public Long getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", userAge=" + userAge +
                '}';
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserAge(int userAge) {
        this.userAge = userAge;
    }

    public int getUserAge() {
        return userAge;
    }
}
