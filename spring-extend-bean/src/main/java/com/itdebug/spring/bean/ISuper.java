package com.itdebug.spring.bean;
/**
 * ISuper
 *
 * @author itdebug
 * @since 2021-12-01
 */
public interface ISuper {

    String getType();
}
