package com.itdebug.spring.bean;

/**
 * @version:: springboot-util-common-more
 * @description:
 * @create: 2022/1/11 19:29.
 */
public class FoodInfo {
    Long foodId;
    private String foodName;

    public Long getFoodId() {
        return foodId;
    }

    public void setFoodId(Long foodId) {
        this.foodId = foodId;
    }

    public String getFoodName() {
        return foodName;
    }

    @Override
    public String toString() {
        return "FoodInfo{" +
                "foodId=" + foodId +
                ", foodName='" + foodName + '\'' +
                '}';
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }
}
