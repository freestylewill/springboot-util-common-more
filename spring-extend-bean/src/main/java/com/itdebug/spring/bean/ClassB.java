package com.itdebug.spring.bean;

import com.itdebug.spring.constant.BeanType;
import org.springframework.stereotype.Component;

/**
 * ClassB
 *
 * @author itdebug
 * @since 2021-12-01
 */
@Component
public class ClassB implements ISuper {

    @Override
    public String getType() {
        return BeanType.CLASS_B.getBeanType();
    }
}
