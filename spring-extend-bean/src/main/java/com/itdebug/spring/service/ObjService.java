package com.itdebug.spring.service;

/**
 * @version:: springboot-util-common-more
 * @description:
 * @create: 2022/1/8 0:50.
 */
public interface ObjService {
    Integer addService(Integer a,Integer b);
    Integer divService(Integer a,Integer b);
}
