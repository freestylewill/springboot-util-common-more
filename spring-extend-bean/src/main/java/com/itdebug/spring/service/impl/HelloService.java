package com.itdebug.spring.service.impl;

import org.springframework.stereotype.Service;

@Service
public class HelloService {
    public void sayHi(String name) {
        System.out.println("hi," + name);
    }
    public void sayHi(String firstName, String lastName) {
        System.out.println("hi," + firstName + lastName);
    }
}