package com.itdebug.spring.service.impl;

import com.itdebug.spring.service.ObjService;
import org.springframework.stereotype.Service;

/**
 * @version:: springboot-util-common-more
 * @description:
 * @create: 2022/1/8 0:51.
 */
@Service
public class BookService implements ObjService {
    @Override
    public Integer addService(Integer a, Integer b) {
        return a+b;
    }
    @Override
    public Integer divService(Integer a, Integer b) {
        return a/b;
    }
}
