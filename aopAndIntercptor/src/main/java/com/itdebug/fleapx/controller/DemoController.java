package com.itdebug.fleapx.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @GetMapping("/test/{name}")
    public String test(@PathVariable String name,
                       @RequestParam String param){
        name = "hello "+name+"  "+param;
        return name;
    }
}
