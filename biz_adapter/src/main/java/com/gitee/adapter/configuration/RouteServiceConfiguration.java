package com.gitee.adapter.configuration;

import com.gitee.adapter.spring.BizRouteServiceProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RouteServiceConfiguration {
    @Bean
    public BizRouteServiceProcessor bizRouteServiceProcessor() {
        return new BizRouteServiceProcessor();
    }
}
