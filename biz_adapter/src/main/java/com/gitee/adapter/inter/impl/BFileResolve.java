package com.gitee.adapter.inter.impl;

import com.gitee.adapter.inter.FileTypeResolveEnum;
import com.gitee.adapter.inter.IFileStrategy;
import org.springframework.stereotype.Component;

@Component
public class BFileResolve implements IFileStrategy {
   
    @Override
    public FileTypeResolveEnum gainFileType() {
        return FileTypeResolveEnum.File_B_RESOLVE;
    }


    @Override
    public void resolve(Object objectparam) {
        System.out.println("B 类型解析文件，参数："+objectparam);
//      logger.info("B 类型解析文件，参数：{}",objectparam);
      //B类型解析具体逻辑
    }
}