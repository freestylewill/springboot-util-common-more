package com.gitee.adapter.inter;

/**
 * @version:: springboot-util-common-more
 * @description:
 * @create: 2022/1/11 0:01.
 */
public class FileTypeResolveEnum {
    public static FileTypeResolveEnum File_A_RESOLVE;
    public static FileTypeResolveEnum File_B_RESOLVE;
    public static FileTypeResolveEnum File_DEFAULT_RESOLVE;

    public static enum File_A_RESOLVE {

        DELETE(0, "删除"), RESERVE(1, "订单预定"), CONFIRM(2, "订单确认"), COMPLETE(3, "订单完成"), CLOSE(4, "订单关闭");

        private File_A_RESOLVE(Integer value, String name) {
            this.value = value;
            this.fileName = name;
        }

        private final Integer value;
        private final String fileName;

        public Integer getValue() {
            return value;
        }

        public String getFileType() {
            return fileName;
        }

    }
}
