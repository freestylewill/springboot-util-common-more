package com.gitee.adapter.inter.impl;

import com.gitee.adapter.inter.FileTypeResolveEnum;
import com.gitee.adapter.inter.IFileStrategy;
import org.springframework.stereotype.Component;

@Component
public class DefaultFileResolve implements IFileStrategy {

    @Override
    public FileTypeResolveEnum gainFileType() {
        return FileTypeResolveEnum.File_DEFAULT_RESOLVE;
    }

    @Override
    public void resolve(Object objectparam) {
        System.out.println("默认类型解析文件，参数："+ objectparam);
        //默认类型解析具体逻辑
    }
}