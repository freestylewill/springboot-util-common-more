package com.itdebug;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpResponseStatus;

/**
 * @author itdebug
 */
public class HTTPRequestHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest msg) throws Exception {

        // 创建要返回的内容
        byte[] retBytes = "this a simple http response".getBytes();
        ByteBuf byteBuf = Unpooled.copiedBuffer(retBytes);
        // 由于http并不是我们关心的重点，我们就直接返回好了
        DefaultHttpResponse response = new DefaultHttpResponse(msg.protocolVersion(), HttpResponseStatus.OK);
        response.headers().set(HttpHeaderNames.CONTENT_LENGTH, retBytes.length);
        ctx.writeAndFlush(response);
        ctx.writeAndFlush(byteBuf);

    }
}

