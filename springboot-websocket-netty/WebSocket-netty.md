# 操作
## 首先，启动我们的服务器。然后打开刚才的那个页面(http://127.0.0.1:8866/)，打开调试模式(f12)。
## 然后输入如下 js 代码

```
var ws = new WebSocket("ws://127.0.0.1:8866/ws");
ws.onopen = function(evt) {
    console.log("链接建立了 ...");
};
ws.onmessage = function(evt) {
    console.log( "收到了消息: " + evt.data);
};
```