package com.fleapx.service;

/**
 * 
 * @author itdebug
 * @describe "日志服务接口"
 * @date 2020年5月22日
 */
public interface LogService {

    void print(String message);
}
