package com.fleapx.dao;

import java.util.List;

public interface UserDAO {
    List<String> getAllUserNames();
}
