package com.fleapx.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;

import com.fleapx.condition.MongoDBDatabaseTypeCondition;
import com.fleapx.condition.MySQLDatabaseTypeCondition;
import com.fleapx.dao.UserDAO;
import com.fleapx.dao.impl.JdbcUserDAO;
import com.fleapx.dao.impl.MongoUserDAO;


//@Configuration
public class DbConfig
{
    @Bean
    @Conditional(MySQLDatabaseTypeCondition.class)
    public UserDAO jdbcUserDAO(){
        return new JdbcUserDAO();
    }

    @Bean
    @Conditional(MongoDBDatabaseTypeCondition.class)
    public UserDAO mongoUserDAO(){
        return new MongoUserDAO();
    }
}