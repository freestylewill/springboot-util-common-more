package com.fleapx.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.fleapx.annotation.DatabaseType;
import com.fleapx.dao.UserDAO;
import com.fleapx.dao.impl.JdbcUserDAO;
import com.fleapx.dao.impl.MongoUserDAO;

@Configuration
@ComponentScan
public class AppConfig
{
	//@Bean
    @DatabaseType("MYSQL")
    public UserDAO jdbcUserDAO(){
        return new JdbcUserDAO();
    }

    @Bean
    @DatabaseType("MONGO")
    public UserDAO mongoUserDAO(){
        return new MongoUserDAO();
    }
}