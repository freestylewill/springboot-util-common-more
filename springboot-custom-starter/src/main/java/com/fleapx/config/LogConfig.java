package com.fleapx.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

import com.fleapx.annotation.LogServiceType;
import com.fleapx.service.LogService;
import com.fleapx.service.impl.FileLogServiceImpl;
import com.fleapx.service.impl.MysqlLogServiceImpl;
import com.fleapx.service.impl.StdOutLogServiceImpl;

//@Configuration
//@ComponentScan
public class LogConfig
{
	@Bean
	@LogServiceType("STDOUT")
	@ConditionalOnMissingBean
    public LogService stdOutLogServiceImpl(){
        return new StdOutLogServiceImpl();
    }

    @Bean
    @LogServiceType("FILE")
    @ConditionalOnMissingBean
    public LogService fileLogServiceImpl(){
        return new FileLogServiceImpl();
    }
    
    @Bean
    @LogServiceType("MYSQL")
    @ConditionalOnMissingBean
    public LogService mysqlLogServiceImpl(){
        return new MysqlLogServiceImpl();
    }
}