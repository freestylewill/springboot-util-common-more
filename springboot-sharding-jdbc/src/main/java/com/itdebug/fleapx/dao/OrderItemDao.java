package com.itdebug.fleapx.dao;

import com.itdebug.fleapx.entity.OrderItem;

import java.util.List;

public interface OrderItemDao {
    /**
     * Description:
     *
     * @param OrderItem orderItem
     * @author itdebug
     * @date 2019/3/25 14:12
     */
    void addOne(OrderItem orderItem);

    /**
     * Description:
     *
     * @param id id
     * @return java.util.List<com.itdebug.fleapx.entity.OrderItem>
     * @author itdebug
     * @date 2019/3/25 14:12
     */
    List<OrderItem> getByOrderId(int id);

    /**
     * Description: 根据订单金额连表查询
     *
     * @author itdebug
     * @date 2019/3/26 16:55
     * @param price
     * @return java.util.List<com.itdebug.fleapx.entity.OrderItem>
     */
    List<OrderItem> getOrderItemByPrice(int price);


}
