package com.itdebug.fleapx.dao;

import com.itdebug.fleapx.entity.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderDao {
    /**
     * Description:
     *
     * @param order order
     * @return long
     * @author itdebug
     * @date 2019/3/25 14:23
     */
    long addOne(Order order);

    long addTwo(Order order);

    /**
     * Description:
     *
     * @param orderId orderId
     * @param userId  userId
     * @return com.itdebug.fleapx.entity.Order
     * @author itdebug
     * @date 2019/3/25 14:23
     */
    Order selectOne(@Param("orderId") long orderId, @Param("userId") int userId);

    /**
     * Description:
     *
     * @param id id
     * @return java.util.List<com.itdebug.fleapx.entity.Order>
     * @author itdebug
     * @date 2019/3/25 14:24
     */
    List<Order> getOrderByUserId(long id);

}
