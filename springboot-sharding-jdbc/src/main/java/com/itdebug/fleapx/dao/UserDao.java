package com.itdebug.fleapx.dao;

import com.itdebug.fleapx.entity.User;

import java.util.List;

public interface UserDao  {
    /**
     * Description:
     *
     * @author itdebug
     * @date 2019/3/25 14:22
     * @param user user
     */
    void addOne(User user);

    /**
     * Description:
     *
     * @author itdebug
     * @date 2019/3/25 14:22
     * @param id id
     * @return com.itdebug.fleapx.entity.User
     */
    User getOneById(long id);

    List<User> selectList(String name);
}
