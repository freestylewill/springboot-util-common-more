package com.itdebug.fleapx.dao;

import com.itdebug.fleapx.entity.Dictionary;

public interface DictionaryDao {

    /**
     * Description:
     *
     * @author itdebug
     * @date 2019/3/26 10:16
     * @param dictionary dictionary
     * @return long
     */
    long addOne(Dictionary dictionary);


}
