package com.itdebug.fleapx.dao;

import com.itdebug.fleapx.entity.OtherTable;

import java.util.List;

public interface OtherTableDao {

    /**
     * Description:
     *
     * @author itdebug
     * @date 2019/3/26 10:16
     * @param dictionary dictionary
     * @return long
     */
    long addOne(OtherTable table);

    /**
     * Description:
     *
     * @author itdebug
     * @date 2019/3/26 16:31
     * @return java.util.List<com.itdebug.fleapx.entity.OtherTable>
     */
    List<OtherTable> getAll();


}
