package com.itdebug.fleapx.entity;

import lombok.Data;

@Data
public class OtherTable {
    private Long id;
    private String name;
}
