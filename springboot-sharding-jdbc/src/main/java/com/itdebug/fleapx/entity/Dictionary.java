package com.itdebug.fleapx.entity;

import lombok.Data;

@Data
public class Dictionary {
    private Long dictionaryId;
    private String name;
    private String value;
}
