package com.itdebug.fleapx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.itdebug.fleapx.dao")
public class FleapxApplication {

    public static void main(String[] args) {
        SpringApplication.run(FleapxApplication.class, args);
    }

}
