package com.fleapx.test;

//import static org.apache.pdfbox.cos.COSName.ST;

import org.junit.Test;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;
import org.stringtemplate.v4.STGroupString;

import java.util.LinkedList;
import java.util.List;

/**
 * @version:: springboot-util-common-more
 * @description:
 * @create: 2022/1/16 14:07.
 */
public class StringTemplateTest {
    @Test
    public void helloMain() {
//        ST hello = new ST("Hello, <model.name>");
//        hello.add("model", new TemplateModel());
//        System.out.println(hello.render());
        ST hello = new ST("Hello, <name>");
        hello.add("name", "World");
        System.out.println(hello.render());
    }
    @Test
    public void riskMain() {
        // 第二个和第三个参数用于定义表达式的头尾字符
        ST hello = new ST("Hello, $if(name)$$name$$endif$", '$', '$');
        hello.add("name", "risk");
        System.out.println(hello.render());
    }
    @Test
    public void sqlMain() {
        STGroup stg = new STGroupString("sqlTemplate(columns,condition) ::= \"select <columns> from table where 1=1 <if(condition)>and <condition><endif> \"");
        ST sqlST = stg.getInstanceOf("sqlTemplate");
        sqlST.add("columns","order_id");
        sqlST.add("condition", "dt='2017-04-04'");
        System.out.print(sqlST.render());
    }
    @Test
    public void sqlMoreMain() {
        STGroup stg = new STGroupFile("src/main/java/com/fleapx/test/dataExtarctSql.stg");
        ST sqlST = stg.getInstanceOf("sqlTemplate");

        List<String> columnList = new LinkedList<String>();
        columnList.add("order_id");
        columnList.add("price");
        columnList.add("phone");
        sqlST.add("columns", columnList);
        sqlST.add("condition", "dt='2017-04-04'");
        System.out.print(sqlST.render());
    }
    @Test
    public void sqlMoreMoreMain() {
        STGroup stg = new STGroupFile("src/main/java/com/fleapx/test/dataMoreExtarctSql.stg");
        ST sqlST = stg.getInstanceOf("sqlTemplate");

        List<String> columnList = new LinkedList<String>();
        columnList.add("order_id");
        columnList.add("price");
        columnList.add("phone");
        columnList.add("user");

        sqlST.add("columns", columnList);
        sqlST.add("condition", "dt='2017-04-04'");
        sqlST.add("joinKey", "user");
        sqlST.add("tableName", "orderTable");

        List<String> childColumnList = new LinkedList<String>();
        childColumnList.add("user");
        childColumnList.add("userLeave");
        childColumnList.add("userLocation");
        sqlST.add("childColumns", childColumnList);
        sqlST.add("childJoinKey", "user");
        sqlST.add("childTableName", "userTable");

        String result = sqlST.render();

        System.out.print(result);
    }
}
