package com.fleapx.moreclumnexecldata.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @ClassName OperationData
 * @Description TODO
 * @Author itdebug
 * @Date 2020/2/14 12:04
 * @Version 1.0
 */
@Data
@EqualsAndHashCode()
@Accessors(chain = true)
public class OperationData {
    /*基础信息*/
    private String supplierName; // 供应商名称
    private String groupNumber; // 供应商组号
    private String firstContYear; // 首次合同签署时间
    private String cardNumber; // 卡号数量
    private String cardPeriod; // 卡号账期
    private String badCardNumber; // 异常状态卡号数量

    /*订单信息*/
    private String orderAmount; // 订货单金额
    private String receiveOrderAmount; // 送货单金额
    private String backOrderAmount; // 退货单金额
    private String orderNumber; // 订货单数量

    /*销售信息*/
    private String saleAmount; // 未税销售金额
    private String conPg; // 综合毛利
    private String netPg; // 净毛利
    private String fee; // 费用
    private String receiveRecord;
    private String saleRecord;
    private String saleAmount90; // 90天销售额（T-1至T-90）
    private String saleAmount180; // 90天销售额（T-91至T-180）
    private String conPg90; // 90天综合毛利（T-1至T-90）

    /*库存信息*/
    private String endInventAm; // 期末库存

    /*保理/融资信息*/
    private String makeLoanNum; // 放款笔数
    private String makeLoanAm; // 放款金额
    private String makeLoanInt; // 放款利息
    private String factFee; // 保理手续费
    private String overdueInt; // 逾期罚息
    private String overdueNum; // 逾期次数
    private String avgMakeLoanAm;
    private String lossNum; // 坏账笔数
}