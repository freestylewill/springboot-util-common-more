﻿$worksheet:{
    $it.rows:{
            <Row>
                <Cell ss:StyleID="s51">
                    <Data ss:Type="String">$it.supplierName$</Data>
                </Cell>
                <Cell ss:StyleID="s51">
                    <Data ss:Type="String">$it.groupNumber$</Data>
                </Cell>
                <Cell ss:StyleID="s52">
                    <Data ss:Type="String">$it.firstContYear$</Data>
                </Cell>
                <Cell ss:StyleID="s53">
                    <Data ss:Type="String">$it.cardNumber$</Data>
                </Cell>
                <Cell ss:StyleID="s53">
                    <Data ss:Type="String">$it.cardPeriod$</Data>
                </Cell>
                <Cell ss:StyleID="s53">
                    <Data ss:Type="String">$it.badCardNumber$</Data>
                </Cell>
                <Cell ss:StyleID="s54">
                    <Data ss:Type="String">$it.orderAmount$</Data>
                </Cell>
                <Cell ss:StyleID="s54">
                    <Data ss:Type="String">$it.receiveOrderAmount$</Data>
                </Cell>
                <Cell ss:StyleID="s54">
                    <Data ss:Type="String">$it.backOrderAmount$</Data>
                </Cell>
                <Cell ss:StyleID="s53">
                    <Data ss:Type="String">$it.orderNumber$</Data>
                </Cell>
                <Cell ss:StyleID="s54">
                    <Data ss:Type="String">$it.saleAmount$</Data>
                </Cell>
                <Cell ss:StyleID="s54">
                    <Data ss:Type="String">$it.conPg$</Data>
                </Cell>
                <Cell ss:StyleID="s54">
                    <Data ss:Type="String">$it.netPg$</Data>
                </Cell>
                <Cell ss:StyleID="s54">
                    <Data ss:Type="String">$it.fee$</Data>
                </Cell>
                <Cell ss:StyleID="s51">
                    <Data ss:Type="String">$it.receiveRecord$</Data>
                </Cell>
                <Cell ss:StyleID="s51">
                    <Data ss:Type="String">$it.saleRecord$</Data>
                </Cell>
                <Cell ss:StyleID="s54">
                    <Data ss:Type="String">$it.saleAmount90$</Data>
                </Cell>
                <Cell ss:StyleID="s54">
                    <Data ss:Type="String">$it.saleAmount180$</Data>
                </Cell>
                <Cell ss:StyleID="s54">
                    <Data ss:Type="String">$it.conPg90$</Data>
                </Cell>
                <Cell ss:StyleID="s54">
                    <Data ss:Type="String">$it.endInventAm$</Data>
                </Cell>
                <Cell ss:StyleID="s53">
                    <Data ss:Type="String">$it.makeLoanNum$</Data>
                </Cell>
                <Cell ss:StyleID="s54">
                    <Data ss:Type="String">$it.makeLoanAm$</Data>
                </Cell>
                <Cell ss:StyleID="s54">
                    <Data ss:Type="String">$it.makeLoanInt$</Data>
                </Cell>
                <Cell ss:StyleID="s54">
                    <Data ss:Type="String">$it.factFee$</Data>
                </Cell>
                <Cell ss:StyleID="s54">
                    <Data ss:Type="String">$it.overdueInt$</Data>
                </Cell>
                <Cell ss:StyleID="s55">
                    <Data ss:Type="String">$it.overdueNum$</Data>
                </Cell>
                <Cell ss:StyleID="s54">
                    <Data ss:Type="String">$it.avgMakeLoanAm$</Data>
                </Cell>
                <Cell ss:StyleID="s55">
                    <Data ss:Type="String">$it.lossNum$</Data>
                </Cell>
            </Row>
            }$
    }$