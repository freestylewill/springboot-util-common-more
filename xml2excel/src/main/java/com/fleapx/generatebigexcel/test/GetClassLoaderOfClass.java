package com.fleapx.generatebigexcel.test;// Java program to demonstrate the example
// of ClassLoader getClassLoader() method of Class 

public class GetClassLoaderOfClass {
    public static void main(String[] args) throws Exception {

        // It returns the Class object attached with the given
        //classname
        Class cl = Class.forName("com.fleapx.generatebigexcel.test.STGroup");

        // By using getClassLoader() is to load the class
        ClassLoader class_load = cl.getClassLoader();

        // If any ClassLoader associate with the Class
        if (class_load != null) {

            Class load_class = class_load.getClass();
            System.out.print("Associated Loader Class: ");
            System.out.print(load_class.getName());
        } else {
            // No Loader associated with the class
            System.out.println("No system loader associated with the class");
        }
    }
}

