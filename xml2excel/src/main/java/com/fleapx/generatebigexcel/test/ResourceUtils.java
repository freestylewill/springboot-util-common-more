package com.fleapx.generatebigexcel.test;

public abstract class ResourceUtils
{  
    public static void main(String[] args)  
    {  
        test();  
    }  
    /** 
     * @param
     */  
    public static void test()  
    {  
        String name = "test.st'";
        System.out.println(null != Thread.currentThread().getContextClassLoader().getResource(name));//相对于classpath根路径  
        System.out.println(null != Thread.currentThread().getContextClassLoader().getResourceAsStream(name));//相对于classpath根路径  
        System.out.println(null != ResourceUtils.class.getResource(name));//相对于ResourceUtils.class文件所在包路径。如果name="/plugins.xml"则是相对于classpath根路径  
        System.out.println(null != ResourceUtils.class.getResourceAsStream(name));//相对于ResourceUtils.class文件所在包路径。如果name="/plugins.xml"则是相对于classpath根路径  
    }  
}  