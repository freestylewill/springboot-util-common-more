package com.fleapx.generatebigexcel.test;
 
import org.antlr.stringtemplate.StringTemplate;
import org.antlr.stringtemplate.StringTemplateGroup;

public class STGroup {
 
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// stGroup --> group name
		// com/siyuan/st/test --> st文件目录(文件系统路径)
//		StringTemplateGroup stGroup = new StringTemplateGroup("stGroup", "com/fleapx/generatebigexcel/test/st/test");
		// test --> st文件名，省略st后缀
//		StringTemplate st = stGroup.getInstanceOf("test");
		// stGroup --> group name
		StringTemplateGroup stGroup = new StringTemplateGroup("stGroup");
		// com/siyuan/st/test/test --> st文件路径，省略st后缀(class路径)
		StringTemplate st = stGroup.getInstanceOf("test");
//		StringTemplate st = stGroup.getInstanceOf("com/siyuan/st/test/test");
		st.setAttribute("attribute", "attr");
		System.out.println(st);
		
	}
 
}