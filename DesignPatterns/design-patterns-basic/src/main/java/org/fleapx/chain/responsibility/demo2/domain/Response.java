package org.fleapx.chain.responsibility.demo2.domain;

/**
 * @author fleapx
 * @create 2:17 PM 05/12/2018
 */
public class Response {

    private String responseStr;

    public String getResponseStr() {
        return responseStr;
    }

    public void setResponseStr(String responseStr) {
        this.responseStr = responseStr;
    }

}
