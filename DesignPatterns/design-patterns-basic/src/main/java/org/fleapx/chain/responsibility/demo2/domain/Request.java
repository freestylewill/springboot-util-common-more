package org.fleapx.chain.responsibility.demo2.domain;

/**
 * @author fleapx
 * @create 2:17 PM 05/12/2018
 */
public class Request {

    private String requestStr;

    public String getRequestStr() {
        return requestStr;
    }

    public void setRequestStr(String requestStr) {
        this.requestStr = requestStr;
    }

}
