package org.fleapx.factory.method;

import org.fleapx.factory.Sender;
import org.fleapx.factory.BluetoothSender;

/**
 * @author fleapx
 * @create 4:37 PM 05/12/2018
 */
public class BluetoothSenderFactory implements SenderFactory {
    @Override
    public Sender createSender() {
        return new BluetoothSender();
    }
}
