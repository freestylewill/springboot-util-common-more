package org.fleapx.factory.abstr;

import org.fleapx.factory.BluetoothSender;
import org.fleapx.factory.Sender;
import org.fleapx.factory.WiFiSender;

/**
 * Created by fleapx on 2018/8/20.
 */
public class SenderFactory extends AbstractFactory {
    @Override
    public Sender createBluetoothSender() {
        return new BluetoothSender();
    }

    @Override
    public Sender createWifiSender() {
        return new WiFiSender();
    }
}
