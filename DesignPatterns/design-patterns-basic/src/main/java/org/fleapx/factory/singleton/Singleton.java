package org.fleapx.factory.singleton;

/**
 * Created by fleapx on 2018/5/14.
 */
public class Singleton {

    private Singleton() {}

    public void doSomething() {
        System.out.println("do some thing ...");
    }

}
