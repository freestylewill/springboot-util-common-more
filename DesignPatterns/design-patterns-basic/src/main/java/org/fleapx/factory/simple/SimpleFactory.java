package org.fleapx.factory.simple;

import org.fleapx.factory.BluetoothSender;
import org.fleapx.factory.Sender;
import org.fleapx.factory.WiFiSender;

/**
 * @author fleapx
 * @create 4:33 PM 05/12/2018
 */
public class SimpleFactory {

    public static Sender createSender(String mode) {
        switch (mode) {
            case "Wi-Fi":
                return new WiFiSender();
            case "Bluetooth":
                return new BluetoothSender();
            default:
                throw new IllegalArgumentException("illegal type: " + mode);
        }
    }

}
