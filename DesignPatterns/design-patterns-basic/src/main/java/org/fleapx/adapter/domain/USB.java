package org.fleapx.adapter.domain;

/**
 * @author: fleapx
 * @date: 2022/1/19 11:35
 * @description:
 */
public interface USB {

    boolean isUsb();

}
