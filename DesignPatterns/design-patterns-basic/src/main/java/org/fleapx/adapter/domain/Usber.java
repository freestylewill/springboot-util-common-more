package org.fleapx.adapter.domain;

import org.fleapx.adapter.domain.USB;

/**
 * @author: fleapx
 * @date: 2022/1/19 11:36
 * @description:
 */
public class Usber implements USB {
    @Override
    public boolean isUsb() {
        System.out.println("这是一个USB接口");
        return true;
    }
}
