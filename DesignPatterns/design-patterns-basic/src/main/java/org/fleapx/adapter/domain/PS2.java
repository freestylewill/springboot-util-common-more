package org.fleapx.adapter.domain;

/**
 * @author: fleapx
 * @date: 2022/1/19 11:36
 * @description:
 */
public interface PS2 {

    boolean isPs2();

}
