package org.fleapx.adapter.objectadapter;

import org.fleapx.adapter.domain.PS2;
import org.fleapx.adapter.domain.Usber;

/**
 * @author: fleapx
 * @date: 2022/1/19 11:55
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        PS2 p = new PS2ToUsberAdapter(new Usber());
        p.isPs2();
    }

}
