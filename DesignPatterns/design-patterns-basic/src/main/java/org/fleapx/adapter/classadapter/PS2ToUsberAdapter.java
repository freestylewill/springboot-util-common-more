package org.fleapx.adapter.classadapter;

import org.fleapx.adapter.domain.PS2;
import org.fleapx.adapter.domain.Usber;

/**
 * @author: fleapx
 * @date: 2022/1/19 11:37
 * @description:
 */
public class PS2ToUsberAdapter extends Usber implements PS2 {
    @Override
    public boolean isPs2() {
        System.out.println("PS2接口转化为了USB接口");
        return isUsb();
    }
}
