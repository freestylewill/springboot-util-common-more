package org.fleapx.decorator.demo1;

/**
 * @author fleapx
 * @create 4:38 PM 07/16/2018
 */
public class Test {

    public static void main(String[] args) {
        ConcreteDecorator cd = new ConcreteDecorator(new ConcreteComponent());
        cd.method();
    }

}
