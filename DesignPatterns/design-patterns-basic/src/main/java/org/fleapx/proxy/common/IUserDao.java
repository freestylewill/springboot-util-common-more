package org.fleapx.proxy.common;

/**
 * @author fleapx
 * @create 9:39 AM 07/13/2018
 */
public interface IUserDao {
    void save();
}
