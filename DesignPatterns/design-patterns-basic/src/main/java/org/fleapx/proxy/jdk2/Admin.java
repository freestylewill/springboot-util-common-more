package org.fleapx.proxy.jdk2;

/**
 * @author fleapx
 * @create 10:31 AM 07/13/2018
 */
public class Admin implements Manager {
    public void doSomething() {
        System.out.println("Admin do something.");
    }
}
