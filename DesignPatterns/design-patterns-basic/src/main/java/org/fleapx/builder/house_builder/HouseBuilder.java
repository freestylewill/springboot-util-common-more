package org.fleapx.builder.house_builder;

/**
 * @Classname HouseBuilder
 * @Description 抽象建造者
 * @Date 2021/3/20 11:49
 * @Created by fleapx
 */

public abstract class HouseBuilder {
    //创建产品对象
    protected House house = new House();

    //生产产品流程
    public abstract void buildGround();

    public abstract void buildWall();

    public abstract void buildRoofed();

    //返回产品对象
    public House getHouse() {
        return house;
    }
}