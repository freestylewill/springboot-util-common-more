package org.fleapx.builder.house_builder;

/**
 * @Classname HouseDirector
 * @Description  工程指挥者
 * @Date 2021/3/20 11:50
 * @Created by fleapx
 */

public class HouseDirector {
    private HouseBuilder houseBuilder;

    public HouseDirector(HouseBuilder houseBuilder) {
        this.houseBuilder = houseBuilder;
    }

    public House build() {
        houseBuilder.buildGround();
        houseBuilder.buildWall();
        houseBuilder.buildRoofed();
        return houseBuilder.getHouse();
    }
}