package org.fleapx.builder.car_builder;

/**
 * @author objcfeng
 * @description 指挥者,指导具体构建者如何构建产品，控制调用先后次序，并向调用者返回完整的产品类
 * @date 2020/11/2
 */
public class CarDirector {
    private CarBuilder builder;

    public CarDirector(CarBuilder builder) {
        this.builder = builder;
    }
    public Car build(){
        builder.buildEngine();
        builder.buildShell();
        builder.buildSteeringWheel();
        builder.buildWheel();
        return builder.getCar();
    }
}