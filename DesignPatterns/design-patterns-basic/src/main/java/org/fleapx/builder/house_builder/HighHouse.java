package org.fleapx.builder.house_builder;

/**
 * @Classname HighHouse
 * @Description 具体建造者
 * @Date 2021/3/20 11:51
 * @Created by fleapx
 */

public class HighHouse extends HouseBuilder {

    @Override
    public void buildGround() {
        house.setGround("100平");
        System.out.println("高楼：打地基");
    }

    @Override
    public void buildWall() {
        house.setWall("50米");
        System.out.println("高楼：砌墙50米");
    }

    @Override
    public void buildRoofed() {
        house.setRoofed("天窗");
        System.out.println("别墅：盖天窗");
    }
}