package org.fleapx.builder.car_builder.best;

import org.fleapx.builder.car_builder.Car;

//建造者
public abstract class CarBuilder {
    //新增创建一个新的car对象
    protected Car car = new Car();
     //其他方法不变
    public abstract void buildWheel();
    public abstract void buildShell();
    public abstract void buildEngine();
    public abstract void buildSteeringWheel();
    //修改返回car对象方法
    public Car getCar(){
        return this.car;
    };
}