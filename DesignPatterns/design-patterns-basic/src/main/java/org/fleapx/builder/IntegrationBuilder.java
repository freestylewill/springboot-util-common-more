package org.fleapx.builder;

import org.fleapx.builder.domain.Member;
import org.fleapx.builder.domain.Order;
import org.fleapx.builder.domain.SelectedPlans;
import org.fleapx.builder.domain.UserProfile;

/**
 * @author fleapx
 * @create 10:16 AM 03/29/2018
 */
public interface IntegrationBuilder {

    UserProfile buildUserProfile() ;

    SelectedPlans buildSelectedPlans();

    Order buildOrder();

    Member buildMember() ;

    IntegrationData buildIntegrationData();

}
