package org.fleapx.builder.house_builder;

/**
 * @Classname Client
 * @Description  产品试用客户端
 * @Date 2021/3/20 11:51
 * @Created by fleapx
 */

public class Client {
    public static void main(String[] args) {
        House house1 = new HouseDirector(new VillaHouse()).build();
        System.out.println(house1);
        System.out.println("============================================");
        House house2 = new HouseDirector(new HighHouse()).build();
        System.out.println(house2);
    }
}