package org.fleapx.builder.house_builder;

/**
 * @Classname House
 * @Description  产品实现类
 * @Date 2021/3/20 11:49
 * @Created by fleapx
 */

public class House {
    private String ground;
    private String wall;
    private String roofed;

    public House() {
    }

    public House(String ground, String wall, String roofed) {
        this.ground = ground;
        this.wall = wall;
        this.roofed = roofed;
    }

    public String getGround() {
        return ground;
    }

    public void setGround(String ground) {
        this.ground = ground;
    }

    public String getWall() {
        return wall;
    }

    public void setWall(String wall) {
        this.wall = wall;
    }

    public String getRoofed() {
        return roofed;
    }

    public void setRoofed(String roofed) {
        this.roofed = roofed;
    }

    @Override
    public String toString() {
        return "House{" +
                "ground='" + ground + '\'' +
                ", wall='" + wall + '\'' +
                ", roofed='" + roofed + '\'' +
                '}';
    }
}