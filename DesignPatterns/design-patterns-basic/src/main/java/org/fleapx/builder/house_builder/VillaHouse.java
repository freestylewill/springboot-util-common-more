package org.fleapx.builder.house_builder;

/**
 * @Classname VillaHouse
 * @Description  具体建造者
 * @Date 2021/3/20 11:51
 * @Created by fleapx
 */

public class VillaHouse extends HouseBuilder {

    @Override
    public void buildGround() {
        house.setGround("200平");
        System.out.println("别墅：打地基");
    }

    @Override
    public void buildWall() {
        house.setWall("10米");
        System.out.println("别墅：砌墙10米");
    }

    @Override
    public void buildRoofed() {
        house.setRoofed("天花板");
        System.out.println("别墅：盖天花板");
    }
}