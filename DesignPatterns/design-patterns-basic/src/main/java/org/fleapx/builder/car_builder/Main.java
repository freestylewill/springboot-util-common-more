package org.fleapx.builder.car_builder;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;

public class Main {
    @Test
    public void mainCarBuilder() {
        CarBuilder builder1=new BmwBuilder();
        CarBuilder builder2=new BenzBuilder();
        CarDirector carDirector = new CarDirector(builder1);
        Car car = carDirector.build();
        System.out.println(car);
    }
}