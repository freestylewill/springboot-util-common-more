package org.fleapx.builder.car_builder.best;

/**
 * @author objcfeng
 * @description 宝马建造者
 * @date 2020/11/2
 */
public class BmwBuilder extends CarBuilder {
    //注意每个给car对象构建组成部分的方法调用的是父类的car对象
    @Override
    public void buildWheel() {
        super.car.setWheel("上好的车轮");
    }

    @Override
    public void buildShell() {
        super.car.setShell("宝马外壳");
    }

    @Override
    public void buildEngine() {
        super.car.setEngine("宝马产发动机");
    }

    @Override
    public void buildSteeringWheel() {
        super.car.setSteeringWheel("有宝马标识的方向盘");
    }
    
    //子类不需要实现getCar方法
    //    @Override
    //    public Car getCar() {
    //        return car;
    //    }
}