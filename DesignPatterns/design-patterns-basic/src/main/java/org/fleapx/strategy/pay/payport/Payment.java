package org.fleapx.strategy.pay.payport;


import org.fleapx.strategy.pay.PayState;

/**
 * 支付渠道(相当于支付策略)
 * Created by fleapx on 2018/9/11.
 */
public interface Payment {

    PayState pay(String uid, double amount);

}
