package org.fleapx.visitor;

/**
 * Created by fleapx on 2019/1/5.
 */
public class WeiboLogin implements Login {
    @Override
    public void accept(Visitor visitor) {
        System.out.println(visitor.getClass().getSimpleName() + "-微博登录");
    }
}
