package defaultmethod;

/**
 * @author: fleapx
 * @date: 2022/1/19 09:34
 * @description:
 */
public interface Additional {

    int add(int a, int b);

}
