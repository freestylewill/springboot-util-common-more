package defaultmethod;

/**
 * @author: fleapx
 * @date: 2022/1/19 09:29
 * @description:
 */
public interface Addition {

    default int add(int a, int b) {
        return a + b;
    }

}
