package org.fleapx.business.identify.component.primary;

import org.fleapx.business.identify.component.domain.IdentifyCriterion;
import org.fleapx.business.identify.component.enums.IdentificationResultType;

/**
 * 组合主键查询接口
 * @author fleapx
 * @create 3:25 PM 09/05/2018
 */
public interface KeyIdentification {

    IdentificationResultType doIdentify(IdentifyCriterion identifyCriterion, KeyIdentificationChain chain);

}
