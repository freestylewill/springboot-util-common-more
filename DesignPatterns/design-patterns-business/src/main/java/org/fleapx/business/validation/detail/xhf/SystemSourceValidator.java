package org.fleapx.business.validation.detail.xhf;

import org.apache.commons.lang.StringUtils;
import org.fleapx.business.domain.detail.XHFRequestDetail;
import org.fleapx.business.domain.file.XHFRequestFile;
import org.fleapx.business.validation.Validator;
import org.fleapx.business.validation.ValidatorChain;
import org.fleapx.business.validation.ValidatorConstants;
import org.fleapx.exception.BusinessValidationException;
import org.springframework.stereotype.Component;

/**
 * @author fleapx
 * @create 2:57 PM 05/09/2018
 */
@Component(ValidatorConstants.BEAN_NAME_XHF_SYSTEM_SOURCE)
public class SystemSourceValidator implements Validator<XHFRequestDetail,XHFRequestFile> {

    public String doValidate(XHFRequestDetail detail, XHFRequestFile file, ValidatorChain chain) throws BusinessValidationException {
        if (StringUtils.isEmpty(detail.getSystemSourceId())) {
            return "System Source ID is required when current flow is GMG integration workflow..";
        }
        return chain.doValidate(detail,file);
    }

}
