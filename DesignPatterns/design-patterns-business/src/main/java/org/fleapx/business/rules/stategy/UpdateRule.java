package org.fleapx.business.rules.stategy;

import org.fleapx.business.rules.domain.CheckResult;

/**
 * @author fleapx
 * @create 3:08 PM 09/30/2018
 */
public interface UpdateRule {
    CheckResult check(String updateStatus, String currentStatus);
}
