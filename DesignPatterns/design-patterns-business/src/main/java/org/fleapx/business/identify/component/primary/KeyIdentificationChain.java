package org.fleapx.business.identify.component.primary;

import org.fleapx.business.enums.WorkflowEnum;
import org.fleapx.business.identify.component.domain.IdentifyCriterion;
import org.fleapx.business.identify.component.enums.IdentificationResultType;

/**
 * 组合主键查询标准接口（责任链模式）
 * @author fleapx
 * @create 3:21 PM 09/05/2018
 */
public interface KeyIdentificationChain {

    IdentificationResultType doIdentify(IdentifyCriterion identifyCriterion, WorkflowEnum workflowId);

    KeyIdentificationChain addIdentification(KeyIdentification identification, WorkflowEnum workflowId);

}
