package org.fleapx.business.validation.facade;

import org.fleapx.business.domain.detail.RequestDetail;
import org.fleapx.business.domain.file.RequestFile;
import org.fleapx.business.enums.WorkflowEnum;
import org.fleapx.business.validation.handler.AbstractValidatorHandler;
import org.springframework.stereotype.Component;

/**
 * @author fleapx
 * @create 4:27 PM 05/17/2018
 */
@Component
public class RequestDetailValidationFacade {

    public String validate(RequestDetail requestDetail, RequestFile requestFile) {
        return accessValidator(requestFile).validate(requestDetail,requestFile);
    }

    private AbstractValidatorHandler accessValidator(RequestFile requestFile) {
        AbstractValidatorHandler requestValidation = AbstractValidatorHandler.accessValidatorHandler(requestFile.getProcessWorkFlow());
        return requestValidation;
    }

}
