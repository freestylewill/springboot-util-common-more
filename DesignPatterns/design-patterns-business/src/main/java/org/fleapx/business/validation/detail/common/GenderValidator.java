package org.fleapx.business.validation.detail.common;

import org.apache.commons.lang.StringUtils;
import org.fleapx.business.domain.detail.RequestDetail;
import org.fleapx.business.domain.file.RequestFile;
import org.fleapx.business.validation.Validator;
import org.fleapx.business.validation.ValidatorConstants;
import org.fleapx.constants.Constants;
import org.fleapx.exception.BusinessValidationException;
import org.springframework.stereotype.Component;

/**
 * @author fleapx
 * @create 2:57 PM 05/09/2018
 */
@Component(ValidatorConstants.BEAN_NAME_GENDER)
public class GenderValidator implements Validator<RequestDetail, RequestFile> {

    @Override
    public boolean doValidate(RequestDetail detail, RequestFile file) throws BusinessValidationException {
        if (StringUtils.isNotEmpty(detail.getGender()) && !"M".equals(detail.getGender()) && !"F".equals(detail.getGender())) {
            String result = "An invalid Gender setting was provided. Accepted Value(s): M, F (M = Male; F = Female).";
            detail.bindValidationResult(Constants.INVALID_GENDER,result);
            return false;
        }
        return true;
    }
}
