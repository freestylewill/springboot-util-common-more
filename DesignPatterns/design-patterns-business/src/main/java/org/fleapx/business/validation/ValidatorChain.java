package org.fleapx.business.validation;

import org.fleapx.business.domain.detail.RequestDetail;
import org.fleapx.business.domain.file.RequestFile;
import org.fleapx.business.enums.WorkflowEnum;
import org.fleapx.exception.BusinessValidationException;

/**
 * 业务校验责任链
 * @author fleapx
 * @create 10:36 AM 05/09/2018
 * @version 1.0
 * @since 1.0
 */
public interface ValidatorChain<T extends RequestDetail,F extends RequestFile> {

    String doValidate(T requestDetail, F requestFile) throws BusinessValidationException;

    ValidatorChain addValidator(Validator validator, WorkflowEnum workflowId);
}
