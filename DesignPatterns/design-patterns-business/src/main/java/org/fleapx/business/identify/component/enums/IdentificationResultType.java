package org.fleapx.business.identify.component.enums;

/**
 * 查询结果类型
 * @author fleapx
 * @create 2:55 PM 09/10/2018
 */
public enum IdentificationResultType {

    SINGLE,
    MULTI,
    NO_MATCH,

    ;

}
