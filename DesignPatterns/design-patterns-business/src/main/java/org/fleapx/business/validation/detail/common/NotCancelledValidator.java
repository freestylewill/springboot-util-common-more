package org.fleapx.business.validation.detail.common;

import org.apache.commons.lang.StringUtils;
import org.fleapx.business.domain.detail.RequestDetail;
import org.fleapx.business.domain.file.RequestFile;
import org.fleapx.business.validation.Validator;
import org.fleapx.business.validation.ValidatorConstants;
import org.fleapx.constants.Constants;
import org.fleapx.exception.BusinessValidationException;
import org.springframework.stereotype.Component;

/**
 * @author fleapx
 * @create 2:57 PM 05/09/2018
 */
@Component(ValidatorConstants.BEAN_NAME_CUSTOMER_REVENUE_NOT_CANCELLED)
public class NotCancelledValidator implements Validator<RequestDetail, RequestFile> {

    @Override
    public boolean doValidate(RequestDetail detail, RequestFile file) throws BusinessValidationException {
        if (StringUtils.isNotEmpty(detail.getNotCancelled())
                && !"1".equals(detail.getNotCancelled())) {
            String result = "An invalid Not Cancelled setting was provided. Accepted Value(s): 1 (1 = Yes).";
            detail.bindValidationResult(Constants.INVALID_NOT_CANCELLED,result);
            return false;
        }

        return true;
    }
}
